package cz.spiffyk.hexaskvorky.core

import cz.spiffyk.hexaskvorky.core.exception.GameStateException
import cz.spiffyk.hexaskvorky.core.vec.Point2i

/**
 * The Hexaskvorky game logic class.
 */
class HexaskvorkyGameLogic(

    /**
     * The number of players of this game.
     */
    override val numPlayers: Int,

    /**
     * The length of the connection that wins the match.
     */
    override val endScore: Int

) : HexaskvorkyGame {

    private var _currentPlayer: Int = 0
    private var _winner: Int? = null
    private var _winningPoints: Set<Point2i>? = null
    private val _gameField: MutableMap<Point2i, Int> = HashMap()


    override var currentPlayer: Int
        private set(v) {
            assertInGame()
            _currentPlayer = v % numPlayers
        }
        get() = _currentPlayer

    override var winner: Int
            private set(v) { _winner = v }
            get() {
                assertFinished()
                return _winner
                    ?: throw IllegalStateException("Game is in finished state but no winner has been set")
            }

    override var winningPoints: Set<Point2i>
        private set(v) { _winningPoints = v }
        get() {
            assertFinished()
            return _winningPoints
                ?: throw IllegalStateException("Game is in finished state but no winning points have been set")
        }

    override var state: HexaskvorkyGame.State = HexaskvorkyGame.State.IN_GAME
        private set

    override val filledHexesCount: Int
        get() = _gameField.size


    /**
     * Increments the ID of the currently playing player, wrapping around according to the number of players.
     *
     * @return the ID of the player whose turn it is after the increment
     *
     * @throws GameStateException if state is not [HexaskvorkyGame.State.IN_GAME]
     */
    fun nextPlayer(): Int {
        assertInGame()
        this.currentPlayer++
        return this.currentPlayer
    }


    /**
     * Sets the value at the specified point.
     *
     * @param point the hex to set the value at
     * @param value the value; may be null to erase the value
     *
     * @throws GameStateException if state is not [HexaskvorkyGame.State.IN_GAME]
     */
    fun setValue(point: Point2i, value: Int?) {
        assertInGame()
        if (value === null) {
            _gameField.remove(point)
        } else {
            _gameField[point] = value
        }
    }


    /**
     * For each hex filled with a value, performs the specified action.
     */
    override fun forEachFilledHex(action: ((Point2i, Int) -> Unit)) {
        _gameField.forEach { (point2i, integer) -> action(point2i, integer) }
    }

    /**
     * Gets the ID of the player who filled the hex (if any) at the specified point.
     *
     * @param point the hex to get the value from
     *
     * @return the ID of the player who filled the hex, or `null` if no player did
     */
    override fun valueAt(point: Point2i): Int? {
        return _gameField[point]
    }

    /**
     * Plays a turn of the game. If the hex on the provided point is empty, fills it with the current player's ID,
     * checks for the winning condition and starts the turn of the next player. If the hex is already filled with an ID,
     * nothing happens.
     *
     * @param point the hex to fill
     *
     * @throws GameStateException if state is not [HexaskvorkyGame.State.IN_GAME]
     *
     * @see .getCurrentPlayer
     * @see .valueAt
     * @see .setValue
     * @see .checkWinningCondition
     */
    override fun play(point: Point2i) {
        assertInGame()
        if (valueAt(point) !== null) {
            return
        }

        setValue(point, currentPlayer)
        checkWinningCondition(point)
        if (state == HexaskvorkyGame.State.IN_GAME) {
            nextPlayer()
        }
    }

    /**
     * Checks if the specified hex (usually right after being filled) makes a winning situation and sets the winner as
     * well as the game state if it does. A winning situation occurs if the hex is a part of a line with the length
     * of at least the value of [.getEndScore].
     *
     * @param point the hex to check
     *
     * @throws GameStateException if state is not [HexaskvorkyGame.State.IN_GAME]
     */
    fun checkWinningCondition(point: Point2i) {
        assertInGame()

        val checkedPlayer = valueAt(point)
            ?: throw IllegalArgumentException("The check starting point cannot be empty!")

        val winPoints = HashSet<Point2i>()

        for (neighbour in HexNeighbour.halfValues) {
            var score = 0
            val directionPoints = ArrayList<Point2i>()
            var checkedPoint = point
            var pointValue: Int?

            do {
                score++
                directionPoints.add(checkedPoint)
                checkedPoint += neighbour.direction
                pointValue = valueAt(checkedPoint)
            } while (pointValue == checkedPlayer)

            val oNeighbour = neighbour.opposite
            checkedPoint = point + oNeighbour.direction
            pointValue = valueAt(checkedPoint)
            while (pointValue == checkedPlayer) {
                score++
                directionPoints.add(checkedPoint)
                checkedPoint += oNeighbour.direction
                pointValue = valueAt(checkedPoint)
            }

            if (score >= endScore) {
                this.winner = checkedPlayer
                this.state = HexaskvorkyGame.State.FINISHED
                winPoints.addAll(directionPoints)
            }
        }

        if (this.state === HexaskvorkyGame.State.FINISHED) {
            this.winningPoints = winPoints
        }
    }


    private fun assertInGame() {
        assertState(HexaskvorkyGame.State.IN_GAME)
    }

    private fun assertFinished() {
        assertState(HexaskvorkyGame.State.FINISHED)
    }

    private fun assertState(state: HexaskvorkyGame.State) {
        if (this.state !== state) {
            throw GameStateException("This operation cannot be performed when the game state is not ${state.name}.")
        }
    }


}
