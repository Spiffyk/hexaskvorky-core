package cz.spiffyk.hexaskvorky.core

import cz.spiffyk.hexaskvorky.core.vec.Point2i

interface HexaskvorkyGame {

    /**
     * The number of players of this game.
     */
    val numPlayers: Int

    /**
     * The length of the connection that wins the match.
     */
    val endScore: Int

    /**
     * ID of the current player.
     */
    val currentPlayer: Int

    /**
     * ID of the player who won the game.
     *
     * @throws cz.spiffyk.hexaskvorky.core.exception.GameStateException if state is not [State.FINISHED]
     */
    val winner: Int

    /**
     * Set of coordinates of the points that won the game.
     *
     * @throws cz.spiffyk.hexaskvorky.core.exception.GameStateException if state is not [State.FINISHED]
     */
    val winningPoints: Set<Point2i>

    /**
     * Current game state.
     */
    val state: State

    /**
     * The number of filled hexes.
     */
    val filledHexesCount: Int

    /**
     * For each hex filled with a value, performs the specified action.
     */
    fun forEachFilledHex(action: (Point2i, Int) -> Unit)

    /**
     * Gets the ID of the player who filled the hex (if any) at the specified point.
     *
     * @param point the hex to get the value from
     *
     * @return the ID of the player who filled the hex, or `null` if no player did
     */
    fun valueAt(point: Point2i): Int?

    /**
     * Plays a turn of the game. If the hex on the provided point is empty, fills it with the current player's ID,
     * checks for the winning condition and starts the turn of the next player. If the hex is already filled with an ID,
     * nothing happens.
     *
     * @param point the hex to fill
     *
     * @throws cz.spiffyk.hexaskvorky.core.exception.GameStateException if state is not [State.IN_GAME]
     *
     * @see currentPlayer
     * @see valueAt
     */
    fun play(point: Point2i)

    enum class State {
        /**
         * The game is waiting to be started. An online game may have this state if the server is waiting for all
         * players to connect.
         */
        WAITING,

        /**
         * The game is running and a player has a turn.
         */
        IN_GAME,

        /**
         * The game has finished and won't change anymore.
         */
        FINISHED,
    }
}
