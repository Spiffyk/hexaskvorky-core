package cz.spiffyk.hexaskvorky.core

import cz.spiffyk.hexaskvorky.core.vec.Point2i

/**
 * Neighboring vectors in a hexagonal grid.
 */
enum class HexNeighbour(
    val direction: Point2i
) {

    TOP_LEFT(Point2i.of(0, -1)),
    TOP_RIGHT(Point2i.of(+1, -1)),
    RIGHT(Point2i.of(+1, 0)),
    BOTTOM_RIGHT(Point2i.of(0, +1)),
    BOTTOM_LEFT(Point2i.of(-1, +1)),
    LEFT(Point2i.of(-1, 0)),
    ;

    companion object {
        val halfValues: Iterable<HexNeighbour> = listOf(TOP_LEFT, TOP_RIGHT, RIGHT)

        // initialize the opposite directions
        // (in a static block because Kotlin does not support forward references for enums)
        init {
            TOP_LEFT._opposite = BOTTOM_RIGHT
            TOP_RIGHT._opposite = BOTTOM_LEFT
            RIGHT._opposite = LEFT
            BOTTOM_RIGHT._opposite = TOP_LEFT
            BOTTOM_LEFT._opposite = TOP_RIGHT
            LEFT._opposite = RIGHT
        }
    }

    private var _opposite: HexNeighbour? = null

    val opposite: HexNeighbour
        get() = _opposite ?: throw IllegalStateException("This is never supposed to happen!")

}
