package cz.spiffyk.hexaskvorky.core.vec

import kotlin.math.sqrt

const val SQRT_3 = 1.7320508075688772f

const val DEFAULT_INSTANCE_CACHE_SIZE = 128

fun sqDist(x1: Float, y1: Float, x2: Float, y2: Float): Float {
    val dx = x2 - x1
    val dy = y2 - y1

    return (dx * dx) + (dy * dy)
}

fun dist(x1: Float, y1: Float, x2: Float, y2: Float): Float = sqrt(sqDist(x1, y1, x2, y2))
