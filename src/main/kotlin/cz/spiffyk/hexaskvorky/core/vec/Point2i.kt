package cz.spiffyk.hexaskvorky.core.vec

import kotlin.math.floor
import kotlin.math.round

class Point2i private constructor(override val x: Int, override val y: Int) : Point2<Int> {

    companion object {
        private val cache = PointCache2(::Point2i)

        fun of(x: Int, y: Int) = cache.get(x, y)

        fun axialToXOffset(axialX: Int, axialY: Int): Float =
            (SQRT_3 * axialX) / 2.0f + (SQRT_3 * axialY) / 4.0f

        fun axialToYOffset(axialX: Int, axialY: Int): Float =
            (3.0f / 4.0f) * axialY

        // TODO - precision is good enough for now but it can be improved
        fun offsetToXAxial(offsetX: Float, offsetY: Float): Int =
            round((2.0f / SQRT_3) * offsetX - (2.0f / 3.0f) * offsetY).toInt()

        // TODO - precision is good enough for now but it can be improved
        fun offsetToYAxial(offsetX: Float, offsetY: Float): Int =
            floor((4.0 / 3.0) * offsetY).toInt()
    }

    operator fun plus(other: Point2i) = Point2i(this.x + other.x, this.y + other.y)

    operator fun minus(other: Point2i) = Point2i(this.x - other.x, this.y - other.y)

    operator fun times(other: Int) = Point2i(this.x * other, this.y * other)

    operator fun unaryPlus() = this

    operator fun unaryMinus() = Point2i(-this.x, -this.y)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Point2i

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x
        result = 31 * result + y
        return result
    }


}
