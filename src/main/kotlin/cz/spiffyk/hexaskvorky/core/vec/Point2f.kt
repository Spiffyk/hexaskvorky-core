package cz.spiffyk.hexaskvorky.core.vec

class Point2f private constructor(override val x: Float, override val y: Float) : Point2<Float> {

    companion object {
        fun of(x: Float, y: Float) = Point2f(x, y)
    }

    operator fun plus(other: Point2f)
            = Point2f(this.x + other.x, this.y + other.y)

    operator fun minus(other: Point2f)
            = Point2f(this.x - other.x, this.y - other.y)

    operator fun times(other: Float)
            = Point2f(this.x * other, this.y * other)

    operator fun unaryPlus()
            = this

    operator fun unaryMinus()
            = Point2f(-this.x, -this.y)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Point2f

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }


}
