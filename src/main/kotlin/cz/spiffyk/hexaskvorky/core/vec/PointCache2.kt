package cz.spiffyk.hexaskvorky.core.vec

import java.util.*
import kotlin.collections.HashMap

class PointCache2<K, V : Point2<K>>(
    private val valueConstructor: (K, K) -> V,
    val maxSize: Int = DEFAULT_INSTANCE_CACHE_SIZE
) {

    companion object {
        const val INFINITE_SIZE = 0
    }

    private val cacheMap = HashMap<K, HashMap<K, V>>()
    private val cacheList = LinkedList<V>()

    fun get(x: K, y: K): V {
        val existingYMap = cacheMap[x]
        if (existingYMap !== null) {
            val existingValue = existingYMap[y]
            if (existingValue !== null) {
                return existingValue
            }
        } else {
            cacheMap[x] = HashMap()
        }

        val newValue = valueConstructor(x, y)
        if (maxSize == INFINITE_SIZE || cacheList.size == maxSize) {
            val removed = cacheList.removeFirst()
            val yMap = cacheMap[removed.x]!!
            yMap.remove(removed.y)
        }
        return newValue
    }

}
