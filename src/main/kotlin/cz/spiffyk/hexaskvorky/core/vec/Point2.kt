package cz.spiffyk.hexaskvorky.core.vec

interface Point2<K> {
    val x: K
    val y: K
}
