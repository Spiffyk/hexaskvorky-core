package cz.spiffyk.hexaskvorky.core

import org.junit.Test

import assertk.assertThat
import assertk.assertions.*
import cz.spiffyk.hexaskvorky.core.exception.GameStateException
import cz.spiffyk.hexaskvorky.core.vec.Point2i
import java.lang.IllegalArgumentException


class HexaskvorkyGameLogicTest {

    @Test
    fun checkWinningCondition1() {
        val game = HexaskvorkyGameLogic(3, 4)

        assertThat(game.state).isEqualTo(HexaskvorkyGame.State.IN_GAME)

        game.setValue(Point2i.of(0, 0), 2)
        game.setValue(Point2i.of(0, 1), 2)
        game.setValue(Point2i.of(0, 2), 2)
        game.setValue(Point2i.of(0, 3), 2)

        game.checkWinningCondition(Point2i.of(0, 2))

        assertThat(game.state).isEqualTo(HexaskvorkyGame.State.FINISHED)
        assertThat(game.winner).isEqualTo(2)
        assertThat(game.winningPoints).containsExactly(
            Point2i.of(0, 0),
            Point2i.of(0, 1),
            Point2i.of(0, 2),
            Point2i.of(0, 3)
        )
    }

    @Test
    fun checkWinningCondition2() {
        val game = HexaskvorkyGameLogic(3, 4)

        assertThat(game.state).isEqualTo(HexaskvorkyGame.State.IN_GAME)

        game.setValue(Point2i.of(0, 0), 1)
        game.setValue(Point2i.of(-1, 1), 1)
        game.setValue(Point2i.of(-2, 2), 1)
        game.setValue(Point2i.of(-3, 3), 1)

        game.checkWinningCondition(Point2i.of(-3, 3))

        assertThat(game.state).isEqualTo(HexaskvorkyGame.State.FINISHED)
        assertThat(game.winner).isEqualTo(1)
        assertThat(game.winningPoints).containsExactly(
            Point2i.of(0, 0),
            Point2i.of(-1, 1),
            Point2i.of(-2, 2),
            Point2i.of(-3, 3)
        )
    }

    @Test
    fun checkWinningCondition3() {
        val game = HexaskvorkyGameLogic(3, 4)

        assertThat(game.state).isEqualTo(HexaskvorkyGame.State.IN_GAME)

        game.setValue(Point2i.of(0, 0), 1)
        game.setValue(Point2i.of(-1, 1), 1)
        game.setValue(Point2i.of(0, 2), 1)
        game.setValue(Point2i.of(-3, 3), 1)

        game.checkWinningCondition(Point2i.of(-3, 3))

        assertThat(game.state).isEqualTo(HexaskvorkyGame.State.IN_GAME)
    }

    @Test
    fun checkWinningCondition_negative1() {
        val game = HexaskvorkyGameLogic(3, 4)

        assertThat { game.checkWinningCondition(Point2i.of(-3, 3)) }
            .isFailure()
            .isInstanceOf(IllegalArgumentException::class.java)
    }

    @Test
    fun play1() {
        val game = HexaskvorkyGameLogic(3, 4)

        assertThat(game.currentPlayer).isEqualTo(0)

        game.play(Point2i.of(0, 1))
        assertThat(game.valueAt(Point2i.of(0, 1)))
            .isEqualTo(0)
        assertThat(game.currentPlayer)
            .isEqualTo(1)

        game.play(Point2i.of(3, 1))
        assertThat(game.valueAt(Point2i.of(3, 1)))
            .isEqualTo(1)
        assertThat(game.currentPlayer)
            .isEqualTo(2)

        game.play(Point2i.of(5, 1))
        assertThat(game.valueAt(Point2i.of(5, 1)))
            .isEqualTo(2)
        assertThat(game.currentPlayer)
            .isEqualTo(0)

        game.play(Point2i.of(0, 10))
        assertThat(game.valueAt(Point2i.of(0, 10)))
            .isEqualTo(0)
        assertThat(game.currentPlayer)
            .isEqualTo(1)
    }

    @Test
    fun play_negative1() {
        val game = HexaskvorkyGameLogic(3, 4)

        assertThat(game.currentPlayer).isEqualTo(0)

        game.play(Point2i.of(0, 1))
        assertThat(game.valueAt(Point2i.of(0, 1)))
            .isEqualTo(0)
        assertThat(game.currentPlayer)
            .isEqualTo(1)

        game.play(Point2i.of(0, 1)) // game should not proceed because the point is already filled
        assertThat(game.valueAt(Point2i.of(0, 1)))
            .isEqualTo(0)
        assertThat(game.currentPlayer)
            .isEqualTo(1)
    }

    @Test
    fun play_negative2() {
        val game = HexaskvorkyGameLogic(3, 4)

        game.setValue(Point2i.of(0, 0), 2)
        game.setValue(Point2i.of(0, 1), 2)
        game.setValue(Point2i.of(0, 2), 2)
        game.setValue(Point2i.of(0, 3), 2)

        game.checkWinningCondition(Point2i.of(0, 2))

        assertThat { game.play(Point2i.of(10, 10)) }
            .isFailure()
            .isInstanceOf(GameStateException::class.java)
    }

    @Test
    fun forEachFilledHex1() {
        val game = HexaskvorkyGameLogic(3, 4)

        game.setValue(Point2i.of(13, 12), 5)
        game.setValue(Point2i.of(23, 55), 1)
        game.setValue(Point2i.of(22, 34), 2)
        game.setValue(Point2i.of(16, 16), 4)
        game.setValue(Point2i.of(15, 51), 0)

        val filledPoints = HashSet<PointData>()
        game.forEachFilledHex { point, value -> filledPoints.add(PointData(point, value)) }
        assertThat(filledPoints).containsExactly(
            PointData(Point2i.of(23, 55), 1),
            PointData(Point2i.of(16, 16), 4),
            PointData(Point2i.of(22, 34), 2),
            PointData(Point2i.of(15, 51), 0),
            PointData(Point2i.of(13, 12), 5),
        )
    }

    private data class PointData (
        val point: Point2i,
        val value: Int
    )

}
