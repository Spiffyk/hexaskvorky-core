package cz.spiffyk.hexaskvorky.core

import assertk.Assert
import assertk.assertions.support.expected

fun Assert<Set<*>>.containsExactly(vararg expected: Any?) = given { actual ->
    if (actual.size != expected.size) {
        expected("size to be:${expected.size} but was:${actual.size}")
    }

    expected.forEach { element ->
        if (!actual.contains(element)) {
            expected("the set to contain $element but it did not")
        }
    }
}
